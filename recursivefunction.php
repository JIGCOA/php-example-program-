<?
//ต้องการหาผลคูณของ ค่า x และ y โดยห้ามใช้ loop / เครื่่องหมายคูณ

/**
 * @param $x
 * @param $y
 */
function recur($x, $y)
{
    if ($x == 0) {
        return 0;
    } else {
        //ถ้า x ยังไม่เท่ากับ0 ให้เรียกใช้ตัวมันเอง
        return $y + recur($x - 1, $y);
    }
}
echo "sum = " . recur(9, 5) . "<br>";
